const sendHttp = require('./sendHttp')
const Promise = require('bluebird')
const LOCALHOST = '127.0.0.1'

class a8Connect {
  constructor({port, hostname}){
    this.port = port || 3036

    //this.hostname is a promise because we need to get it from the dns some of the time
    this.hostname = hostname
  }

  invoke(fullyQualifiedName, inputs){
    const pArray = fullyQualifiedName.split('.')
    const appName = pArray[0]
    const functionName = pArray[1]

    const path = `/invoke/${appName}/${functionName}`
    const data = !inputs ? {} : inputs
    const headers = { "Content-Type" : "application/json" }

    return sendHttp({method: 'POST', hostname : this.hostname, port: this.port, path, data, headers})
      .then((response)=>{

        return JSON.parse(response)
        //return response
      })
      .catch((error)=>{
        return error
      })
  }

  listApps(){
    return new Promise((resolve, reject) => {
      const path =  `/apps`

      return sendHttp({method: 'GET', hostname: this.hostname, port: this.port, path})
        .then((response) => {
          const jsonResponse = JSON.parse(response)
          if(Array.isArray(jsonResponse)){
            resolve(jsonResponse)
          }else{
            reject(jsonResponse)
          }
        })
        .catch((error)=>{
          resolve(error)
        })
    })
  }

  listFunctions(appName){
    return new Promise((resolve, reject) => {
      const path = `/fn/${appName}`

      return sendHttp({method: 'GET', hostname: this.hostname, port: this.port, path})
        .then((response) => {
          const jsonResponse = JSON.parse(response)
          if (Array.isArray(jsonResponse)) {
            resolve(jsonResponse)
          } else {
            reject(jsonResponse)
          }
        })
    })
  }
}

module.exports = a8Connect