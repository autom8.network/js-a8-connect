const http = require('http')

const sendHttpRequest = function ({method, hostname, port, path, parameters = {}, data, headers = {}}){

  const queryString = Object.keys(parameters).map((key)=>{
    return `${key}=${parameters[key]}`
  }).join('&')

  return new Promise((resolve, reject) => {
    const options = {
      hostname: hostname,
      port: port,
      path: `${path}?${queryString}`
      ,method: method.toUpperCase()
    }

    const req = http.request(options, (res) => {

      res.setEncoding('utf8')
      let responseData = ""
      res.on('data', (chunk) => {
        responseData +=  chunk
      })
      res.on('end', () => {

        resolve(responseData)
      })
    })

    Object.keys(headers).forEach((name) => {
      req.setHeader(name, headers[name])
    })

    req.on('error', (e) => {
      reject(e)
    })

    if(data){
      req.write(JSON.stringify(data))
    }

    req.end()
  })
}

module.exports = sendHttpRequest
